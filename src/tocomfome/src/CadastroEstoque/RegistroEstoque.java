
package CadastroEstoque;


public class RegistroEstoque {
    private String tipoAlimento;
    private String alimento;
    private String preco;
    private int quantidade;
    
    public RegistroEstoque(String tipoAlimento, String alimento, String preco,int quantidade) {
        this.tipoAlimento = tipoAlimento;
        this.alimento = alimento;
        this.preco = preco;
        this.quantidade= quantidade;
    }

    public String getTipoAlimento() {
        return tipoAlimento;
    }

    public void setTipoAlimento(String tipoAlimento) {
        this.tipoAlimento = tipoAlimento;
    }

    public String getAlimento() {
        return alimento;
    }

    public void setAlimento(String alimento) {
        this.alimento = alimento;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }
    public int getQuantidade(){
        return quantidade;
    }
    
    
    public void setQuantidade(int quantidade){
        this.quantidade=quantidade;
    }


}
