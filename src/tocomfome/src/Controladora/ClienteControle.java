
package Controladora;

import CadastroCliente.RegistroCliente;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class ClienteControle {

    public ClienteControle() {
    }
    
    public ArrayList<RegistroCliente> getList(){
        
        ArrayList<RegistroCliente> listCliente = new ArrayList();
        
        try {
            String nomeArquivo="cadastroCliente.txt";
            File arquivo = new File(nomeArquivo);

            FileInputStream fis =new FileInputStream(arquivo);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferDeArquivo= new BufferedReader(isr);

            String linha = bufferDeArquivo.readLine().replace("[", "").replace("]", "");

            while(linha!=null){
                String[] informacao = linha.split(",");

                String nome = informacao[0];
                String tel = informacao[1];
                
                RegistroCliente cliente = new RegistroCliente(nome,tel);
                listCliente.add(cliente);
                
                linha=bufferDeArquivo.readLine();
            }
            
            return listCliente;

        } catch(Exception e) {
            return null;
        } 
    }
    
    public ArrayList<Object[]> toListObject(ArrayList<RegistroCliente> list){
        ArrayList<Object[]> listObj = new ArrayList<>();
        
        for (RegistroCliente registroCliente : list) {
            Object[] obj = { registroCliente.getNome(), registroCliente.getTelefone()};
            listObj.add(obj);
        }
        
        return listObj;
    }
    
    public RegistroCliente findCliente(String pesquisa){
        
        ArrayList<RegistroCliente> list = getList();
        
        for (RegistroCliente registroCliente : list) {
            if(pesquisa.equals(registroCliente.getNome())){
                return registroCliente;
            }
                
        }
        
        return null;
    }
    
}
