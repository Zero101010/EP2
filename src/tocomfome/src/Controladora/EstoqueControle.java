/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladora;

import CadastroEstoque.RegistroEstoque;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 *
 * @author zero101010
 */
public class EstoqueControle {

    public EstoqueControle() {
    }
    
    public ArrayList<RegistroEstoque> getList(){
        ArrayList<RegistroEstoque> list = new ArrayList();
        
        try {
            String nomeArquivo="cadastroEstoque.txt";
            File arquivo = new File(nomeArquivo);

            FileInputStream fis =new FileInputStream(arquivo);
            InputStreamReader isr = new InputStreamReader(fis);
            BufferedReader bufferDeArquivo= new BufferedReader(isr);

            String linha = bufferDeArquivo.readLine().replace("[", "").replace("]", "");
            
            while(linha!=null){
                String[] informacao = linha.split(",");

                String tipoAlimento = informacao[0].trim();
                String alimento = informacao[1];
                String preco = informacao[2];
                String qtdeStr = informacao[3].trim();
                
                int qtde = Integer.parseInt(qtdeStr);
                
                RegistroEstoque estoque = new RegistroEstoque(tipoAlimento, alimento, preco, qtde);
                
                list.add(estoque);
                
                linha=bufferDeArquivo.readLine();
            }
            
            return list;

        } catch(IOException | NumberFormatException e) {
            return null;
        } 
    }
    
    public ArrayList<Object[]> toListObject(ArrayList<RegistroEstoque> list){
        ArrayList<Object[]> listObj = new ArrayList<>();
        
        for (RegistroEstoque registro : list) {
            Object[] obj = { registro.getTipoAlimento(), registro.getAlimento(), registro.getPreco(), registro.getQuantidade()};
            listObj.add(obj);
        }
        
        return listObj;
    }
}
